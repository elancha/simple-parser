CFLAGS+=-g -O0 -Wall -Wextra -pedantic -std=c23
LDFLAGS+=

.PHONY: all clean

all: parser

parser: parser.c
	$(CC) $(CFLAGS) -o parser parser.c $(LDFLAGS)

clean:
	rm -rf parser
