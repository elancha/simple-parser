#include <assert.h>
#include <errno.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

typedef struct {
	char *data;
	size_t len;
} SV;

void
sv_append(SV *a, char b)
{
	a->data = realloc(a->data, (a->len + 1)*sizeof(char));
	a->data[a->len] = b;
	a->len += 1;
}

int
sv_equals(SV a, SV b)
{
	if (a.len != b.len)
		return 0;
	for (size_t i = 0; i < a.len; i++) {
		if (a.data[i] != b.data[i])
			return 0;
	}
	return 1;
}

#define SV_LIT(x) (SV){ .data = (x), .len = sizeof(x) - 1 }
#define SV_Fmt "%.*s"
#define SV_Arg(sv) (int)(sv).len, (sv).data

typedef struct {
	size_t row, col;
	SV path;
} Location;

#define log_error(loc, fmt, ...) fprintf(stderr, SV_Fmt ":%d:%d: " fmt "\n", SV_Arg((loc).path), (int)(loc).row, (int)(loc).col __VA_OPT__(,) __VA_ARGS__)

typedef struct {
	Location loc;
	SV raw_data;
} Lexer;

#define PRECEDENCE_MASK 0x00f
#define OPERATOR_MASK   0x100
#define IMPLICIT_MULT_MASK 0x20

typedef enum {
	TOKEN_TYPE_EOF       = 0x000,
	TOKEN_TYPE_ID        = 0x021,
	TOKEN_TYPE_LIT       = 0x022,
	TOKEN_TYPE_DIFF      = 0x004,

	TOKEN_TYPE_PLUS      = 0x102,
	TOKEN_TYPE_MINUS     = 0x142,
	TOKEN_TYPE_TIMES     = 0x103,
	TOKEN_TYPE_DIV       = 0x113,
	TOKEN_TYPE_POWER     = 0x105,

	TOKEN_TYPE_OPEN_PAR  = 0x220,
	TOKEN_TYPE_CLOSE_PAR = 0x201,

	TOKEN_TYPE_COMMA     = 0x400,

	TOKEN_TYPE_EQUALS    = 0x800,
} Token_Type;

typedef struct {
	Token_Type type;
	Location loc;
	SV value;
} Token;


char
peek(Lexer *p)
{
	if (p->raw_data.len <= 0)
		return EOF;
	return *(p->raw_data.data);
}

char
consume(Lexer *p)
{
	if (p->raw_data.len <= 0)
		return EOF;
	char r = *(p->raw_data.data);

	p->loc.col++;
	if (r == '\n') {
		p->loc.col = 0;
		p->loc.row++;
	}

	p->raw_data.len--;
	p->raw_data.data++;

	return r;
}

int
is_alpha(char c)
{
	return (c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z');
}

int
is_digit(char c)
{
	return c >= '0' && c <= '9';
}

int
is_alphanum(char c)
{
	return is_alpha(c) || is_digit(c);
}

int
is_space(char c)
{
	return c == ' ' || c == '\n' || c == '\t';
}

int
single_char_token(Lexer *l, Token *t, char c, Token_Type type)
{
	if (peek(l) == c) {
		t->type = type;
		sv_append(&t->value, consume(l));
		return 1;
	}
	return 0;
}

Token
next_token(Lexer *l)
{
	Token token = {0};

	while ( is_space(peek(l)) ) {
		consume(l);
	}

	token.loc = l->loc;

	if ( is_alpha(peek(l)) ) {
		do {
			sv_append(&token.value, consume(l));
		} while ( is_alphanum(peek(l)) );

		if (sv_equals(token.value, SV_LIT("diff"))) {
			token.type = TOKEN_TYPE_DIFF;
			return token;
		}

		token.type = TOKEN_TYPE_ID;
		return token;
	}

	if (is_digit(peek(l)) || peek(l) == '.') {
		token.type = TOKEN_TYPE_LIT;
		do {
			sv_append(&token.value, consume(l));
		} while ( is_alphanum(peek(l)) || peek(l) == '.');

		return token;
	}

	if (single_char_token(l, &token, '+', TOKEN_TYPE_PLUS     )) return token;
	if (single_char_token(l, &token, '-', TOKEN_TYPE_MINUS    )) return token;
	if (single_char_token(l, &token, '*', TOKEN_TYPE_TIMES    )) return token;
	if (single_char_token(l, &token, '/', TOKEN_TYPE_DIV      )) return token;
	if (single_char_token(l, &token, '^', TOKEN_TYPE_POWER    )) return token;
	if (single_char_token(l, &token, '(', TOKEN_TYPE_OPEN_PAR )) return token;
	if (single_char_token(l, &token, ')', TOKEN_TYPE_CLOSE_PAR)) return token;

	if (peek(l) == '#') {
		char c;
		do {
			c = consume(l);
		} while (c != EOF && c != '\n');

		return next_token(l); 
	}

	if (peek(l) == EOF) {
		token.type = TOKEN_TYPE_EOF;
		return token;
	}

	/* UNKNOWN token */
	log_error(token.loc, "Unexpected char: %c (%d)", peek(l), peek(l)); 
	token.type = TOKEN_TYPE_EOF;
	consume(l);
	return token;
}

Lexer
lexer_from_file(char *path)
{
	FILE *f = fopen(path, "r");
	if (f == NULL) goto file_read_error;
	if (fseek(f, 0, SEEK_END) < 0) goto file_read_error;
	long size = ftell(f);
	if (size < 0) goto file_read_error;
	if (fseek(f, 0, SEEK_SET) < 0) goto file_read_error;

	Lexer l = {0};
	l.raw_data.data = (char *)malloc(size * sizeof(char));
	l.raw_data.len = size;
	l.loc.path.data = path;
	l.loc.path.len = strlen(path);

	fread(l.raw_data.data, size, sizeof(char), f);
	/* FIXME: ferror does not set errno */
	if (ferror(f)) goto file_read_error;

	return l;

file_read_error:
	fprintf(stderr, "Could not read file '%s': %s\n", path, strerror(errno));
	return (Lexer){0};
}


typedef struct {
	Lexer lexer;
	Token processed_token;
} Parser;

Token
token_peek(Parser *p)
{
	if (p->processed_token.type) {
		p->processed_token = next_token(&p->lexer);
	}
	return p->processed_token;
}

Token
token_consume(Parser *p)
{
	Token t = token_peek(p);
	p->processed_token.type = 0;
	return t;
}

typedef enum {
	NODE_TYPE_PLUS,
	NODE_TYPE_MINUS,
	NODE_TYPE_TIMES,
	NODE_TYPE_DIV,
	NODE_TYPE_POWER,
	NODE_TYPE_FCALL,
	NODE_TYPE_EQUALS,

	NODE_TYPE_DIFF,
	NODE_TYPE_ID,
	NODE_TYPE_LIT,
} Node_Type;

typedef struct AST_Node {
	Node_Type type;
	Token token;
	size_t child_count;
	struct AST_Node **childs;
} AST_Node;

AST_Node *parse_expr(Parser *p);
AST_Node *parse_primary(Parser *p);

AST_Node *
parse_lit(Parser *p)
{
	Token t = token_consume(p);
	assert(t.type == TOKEN_TYPE_LIT);
	AST_Node *r = malloc(sizeof(AST_Node));
	/* TODO: value */
	r->type = NODE_TYPE_LIT;
	r->child_count = 0;
	return r;
}

AST_Node *
parse_id(Parser *p)
{
	Token id = token_consume(p);
	assert(id.type == TOKEN_TYPE_ID);

	AST_Node *r = malloc(sizeof(AST_Node));

	Token c = token_peek(p);
	if (c.type != TOKEN_TYPE_OPEN_PAR) {
		/* FIXME: id value */
		r->type = NODE_TYPE_ID;
		r->child_count = 0;
		return r;
	}

	/* TODO: Not compatible with implicit multiply */
	token_consume(p);
	if (token_peek(p).type != TOKEN_TYPE_CLOSE_PAR) {
		while (1) {
			AST_Node *expr = parse_expr(p);
			if (expr) {
				/* FIXME: TODO */
			} else {
				free(r);
				return NULL;
			}

			if (token_peek(p).type == TOKEN_TYPE_CLOSE_PAR)
				break;

			Token c = token_peek(p);
			if (c.type != TOKEN_TYPE_COMMA) {
				log_error(c.loc, "Expected ')' or ',' but got '" SV_Fmt "'", SV_Arg(c.value)); 
			}
			token_consume(p);
		}
	}

	token_consume(p);

	return r;
}

AST_Node *parse_paren_expr(Parser *p)
{
	assert(token_peek(p).type == TOKEN_TYPE_OPEN_PAR);
	AST_Node *expr = parse_expr(p);
	if (!expr)
		return NULL;
	Token cp = token_peek(p);
	if (cp.type != TOKEN_TYPE_CLOSE_PAR) {
		log_error(cp.loc, "Expected ')' but got '" SV_Fmt "'", SV_Arg(cp.value));
	}
	token_consume(p);
	return expr;
}

AST_Node *
parse_primary(Parser *p)
{
	Token t = token_peek(p);
	switch (t.type) {
	case TOKEN_TYPE_ID:
		return parse_id(p);
	case TOKEN_TYPE_LIT:
		return parse_lit(p);
	case TOKEN_TYPE_OPEN_PAR:
		return parse_paren_expr(p);
	default:
		log_error(t.loc, "Unexpected '" SV_Fmt "'", SV_Arg(t.value));
		return NULL;
	}
}

AST_Node *
parse_binary_operation_rhs(Parser *p, AST_Node *lhs, int expression_precedence)
{
	while (1) {
		int this_precedence = (token_peek(p).type & PRECEDENCE_MASK);

		if (this_precedence < expression_precedence)
			return lhs;

		Token bin_op = token_consume(p);
		AST_Node *rhs = parse_primary(p);
		if (!rhs)
			return NULL;

		int next_precedence = (token_peek(p).type & PRECEDENCE_MASK);
		if (this_precedence < next_precedence) {

		}
		AST_Node *r = malloc(sizeof(AST_Node));
		r->type = 0; /* TODO */
		r->child_count = 2;
		r->childs = malloc(sizeof(AST_Node *)*2);
		r->childs[0] = lhs;
		r->childs[1] = rhs;
	}
}

AST_Node *
parse_expr(Parser *p)
{
	AST_Node *lhs = parse_primary(p);
	if (!lhs)
		return NULL;
	return parse_binary_operation_rhs(0, lhs);
}

void
print_node(AST_Node *n, SV indent)
{
	SV node_name;
	switch (n->type) {
	case NODE_TYPE_EQUALS:
		node_name = SV_LIT("=");
		break;
	case NODE_TYPE_FCALL:
		node_name = SV_LIT("fcall");
		break;
	case NODE_TYPE_DIFF:
		node_name = SV_LIT("diff");
		break;
	case NODE_TYPE_ID:
		node_name = SV_LIT("id");
		break;
	case NODE_TYPE_LIT:
		node_name = SV_LIT("lit");
		break;
	case NODE_TYPE_PLUS:
		node_name = SV_LIT("+");
		break;
	case NODE_TYPE_MINUS:
		node_name = SV_LIT("-");
		break;
	case NODE_TYPE_TIMES:
		node_name = SV_LIT("*");
		break;
	case NODE_TYPE_DIV:
		node_name = SV_LIT("/");
		break;
	case NODE_TYPE_POWER:
		node_name = SV_LIT("^");
		break;
	}

	printf(SV_Fmt SV_Fmt "(" SV_Fmt ")\n", SV_Arg(indent), SV_Arg(node_name), SV_Arg(n->token.value));
	SV ii = (SV) {
		.len  = indent.len + 2,
		.data = malloc((indent.len + 2)*sizeof(char)),
	};
	for (size_t i = 0; i < n->child_count; i++) {
		print_node(n->childs[i], ii);
	}

	free(ii.data);
}

int
main(int argc, char *argv[])
{
	(void)argc;
	(void)argv;

	char *path = "input";

	Parser p;
	p.lexer = lexer_from_file(path);
	AST_Node *n = parse_expr(&p);
	print_node(n, SV_LIT(""));

	return 0;
}
